package conversorXML;

import java.io.IOException;
import java.io.Reader;

import javax.xml.bind.JAXBException;

public interface IConversorXML<T> {

	T read (Class<T> clazz, Reader reader) throws IOException, JAXBException;
	
	String parseToString (T t) throws JAXBException;
	
} 
