package conversorXML;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class ConversorXML<T> extends Conversor<T> implements IConversorXML<T>, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ConversorXML() {
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	@Override
	public T read (Class<T> clazz, Reader reader) throws IOException, JAXBException {
		// TODO Auto-generated method stub		

		JAXBContext context = JAXBContext.newInstance(new Class[] { clazz });
        Unmarshaller unmarshaller = context.createUnmarshaller();
        
        T t = (T) unmarshaller.unmarshal(reader);
		
		return t;
	}

	@Override
	public String parseToString (T t) throws JAXBException {
		// TODO Auto-generated method stub
		
		StringWriter stringWriter = new StringWriter();
		
		JAXBContext context = JAXBContext.newInstance(new Class[] { t.getClass() });
		Marshaller marshaller = context.createMarshaller();
		
		marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		marshaller.marshal(t, stringWriter);
		
		return stringWriter.toString();
		
	}
	
}
