package entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="root")
public class Root {
	
	@XmlElement(name="concurso")
	private ConfiraLoteria confiraLoteria;

	public Root() {
		// TODO Auto-generated constructor stub
	}

	/*public ConfiraLoteria getConfiraLoteria() {
		return confiraLoteria;
	}

	public void setConfiraLoteria(ConfiraLoteria confiraLoteria) {
		this.confiraLoteria = confiraLoteria;
	}*/
 
	@Override
	public String toString() {
		return "Root [confiraLoteria=" + confiraLoteria + "]";
	}

	
}
