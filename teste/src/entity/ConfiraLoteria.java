package entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ConfiraLoteria {

	@XmlElement(name="numero")
	private String numero;
	
	@XmlElement(name="data")
	private String data;
	
	@XmlElement(name="dezenas")
	private String dezenas; 
	
	public ConfiraLoteria() {
		// TODO Auto-generated constructor stub
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getDezenas() {
		return dezenas;
	}

	public void setDezenas(String dezenas) {
		this.dezenas = dezenas;
	}

	@Override
	public String toString() {
		return "ConfiraLoteria [numero=" + numero + ", data=" + data
				+ ", dezenas=" + dezenas + "]";
	}
	
}
