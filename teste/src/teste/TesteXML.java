package teste;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import conversorXML.Conversor;
import conversorXML.ConversorXML;
import conversorXML.IConversorXML;
import entity.Root;

public class TesteXML {
	
	URL url;
	
	public TesteXML() throws MalformedURLException {
		// TODO Auto-generated constructor stub
		this.url = new URL("http://confiraloterias.com.br/api/xml/?loteria=lotofacil&token=XyEHIdIGjC70s3r");
	}
	
	private String getXmlString () {
		
		StringBuilder xml = new StringBuilder();
		
		xml.append("<?xml version=\"1.0\" encoding=\"utf-8\"?> ");
		xml.append("<root> \n");
		xml.append("<concurso> \n");
		xml.append("<numero>1077</numero> \n");
		xml.append("<data>05/07/2014</data> \n");
		xml.append("<cidade>EUN�POLIS - BA</cidade> \n");
		xml.append("<local>Caminh�o da Sorte</local> \n");
		xml.append("<dezenas>01|02|05|06|07|08|10|12|13|14|15|19|20|21|24</dezenas> \n");
		xml.append("<valor_acumulado>0</valor_acumulado> \n");
		xml.append(" <premiacao> \n");
		xml.append("<acertos_15> \n");
		xml.append("<ganhadores>3</ganhadores> \n");
		xml.append("<valor_pago>532.018,11</valor_pago> \n");
		xml.append("</acertos_15> \n");
		xml.append("<acertos_14> \n");
		xml.append("<ganhadores>549</ganhadores> \n");
		xml.append("<valor_pago>1.277,89</valor_pago> \n");
		xml.append("</acertos_14> \n");
		xml.append("<acertos_13> \n");
		xml.append("<ganhadores>15166</ganhadores> \n");
		xml.append("<valor_pago>15,00</valor_pago> \n");
		xml.append("</acertos_13> \n");
		xml.append("<acertos_12> \n");
		xml.append("<ganhadores>178761</ganhadores> \n");
		xml.append("<valor_pago>6,00</valor_pago> \n");
		xml.append("</acertos_12> \n");
		xml.append("<acertos_11> \n");
		xml.append("<ganhadores>1020398</ganhadores> \n");
		xml.append("<valor_pago>3,00</valor_pago> \n");
		xml.append("</acertos_11> \n");
		xml.append("</premiacao> \n");
		xml.append("<arrecadacao_total>17.876.455,50</arrecadacao_total> \n");
		xml.append("</concurso> \n");
		xml.append("<proximo_concurso> \n");
		xml.append("<data>07/07/2014</data> \n");
		xml.append("<valor_estimado>1.700.000,00</valor_estimado> \n");
		xml.append("</proximo_concurso> \n");
		xml.append("<especial_independencia_valor_acumulado>47.539.084,06</especial_independencia_valor_acumulado> \n");			  
		xml.append("</root> \n");	
		
		return xml.toString();
	}

	private void lerXMLComJAXBContext_erro_01 () throws IOException, JAXBException {		
			
		Reader reader = new InputStreamReader(url.openStream());
		
		JAXBContext context = JAXBContext.newInstance(new Class[] { Root.class });
        Unmarshaller unmarshaller = context.createUnmarshaller();
        
        BufferedReader bufferedReader = new BufferedReader(reader);
        Iterator<String> iterator = bufferedReader.lines().iterator();
        
        while (iterator.hasNext()) {
        	
        	System.out.println(iterator.next());
        	
        }
        
        Root root = (Root)unmarshaller.unmarshal(reader);
        System.out.println(root);
		
	}

	private Root lerXMLComJAXBContextCorreto () throws IOException {		
			
	//	System.out.println(this.getXmlString());
		StringReader stringReader = new StringReader(this.getXmlString());
		Root root = null;
		try {
			
			JAXBContext context = JAXBContext.newInstance(new Class[] { Root.class });
	        Unmarshaller unmarshaller = context.createUnmarshaller();
	        
	        root = (Root)unmarshaller.unmarshal(stringReader);
	        System.out.println(root);
	        
	        this.gerarXMLComJAXBContextCorreto(root);
	        
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
		return root; 
		
	}
	
	private void gerarXMLComJAXBContextCorreto (Root root) {
		
		try {
			
			StringWriter stringWriter = new StringWriter();
			
			JAXBContext context = JAXBContext.newInstance(new Class[] { Root.class });
			Marshaller marshaller = context.createMarshaller();
			
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.marshal(root, stringWriter);
			
			System.out.println("\n\n\n gerarXMLComJAXBContext: \n " + root);
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private Root lerXMLComJAXBContext () throws IOException, JAXBException {
		
		StringReader stringReader = new StringReader(this.getXmlString());
			
		IConversorXML<Root> conversorXML = new ConversorXML<Root>();
		Root root = conversorXML.read(Root.class, stringReader);
				
		
		/*@SuppressWarnings("unchecked")
		IConversorXML<Root> conversorXML = (IConversorXML<Root>) Conversor.getInstance(Conversor.TYPE.XML);
		Root root = conversorXML.read(Root.class, stringReader);
		*/
		return root;
	}
	
	private void gerarXMLComJAXBContext (Root root) throws JAXBException {
		
		IConversorXML<Root> conversorXML = new ConversorXML<Root>();
		String s = conversorXML.parseToString(root);
				
		System.out.println(s);
		
	}
	
	public static void main(String[] args) throws IOException, JAXBException {

		TesteXML testeXML = new TesteXML();
		Root root = testeXML.lerXMLComJAXBContext();
		
		System.out.println(root);
		
		testeXML.gerarXMLComJAXBContext(root);
		
	}
	
}
